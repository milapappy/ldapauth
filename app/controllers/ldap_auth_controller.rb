# frozen_string_literal: true

require 'net-ldap'

class LdapAuthController < ApplicationController
  $HOST = 'localhost'
  $PORT = '10389'
  $TREEBASE = 'ou=users,dc=example,dc=com'

  # @return [Object]
  def check_ldap_user
    render json: {message: 'user logged in'}, status: 200 if (params[:email].eql? get_mail) && (params[:uid].eql? get_uid)
  end

  def user_role
    get_user_role.each do |role|
      user_by_role = role['member'].to_s
      render json: {role: role['cn'][0]}, status: 200 if user_by_role.include? params[:uid]
    end
  end

  # @return [Object]
  def get_ldap_user
    render json: find_user_by_uid, status: 200
  end

  def find_user_by_uid
    ldap = Net::LDAP.new
    ldap.host = $HOST
    ldap.port = $PORT
    ldap.bind
    tree_base = $TREEBASE
    filter_uid = Net::LDAP::Filter.eq('uid', params[:uid])
    attrs = []
    ldap.search(base: tree_base, attributes: attrs, filter: filter_uid, return_result: true) do |entry|
      return entry if entry
    end
  end

  # @return [Object]
  def find_user
    search_result = {}
    ldap = Net::LDAP.new
    ldap.host = $HOST
    ldap.port = $PORT
    ldap.bind
    tree_base = $TREEBASE
    filter_uid = Net::LDAP::Filter.eq('uid', params[:uid])
    attrs = []
    ldap.search(base: tree_base, attributes: attrs, filter: filter_uid, return_result: true) do |entry|
      search_result = { uid: entry.uid, mail: entry.mail } if entry
      return search_result
    end
  end

  # @return [Object]
  def get_uid
    find_user[:uid][0].to_s
  end

  # @return [Object]
  def get_mail
    find_user[:mail][0].to_s
  end

  def get_user_role
    search_result = {}
    ldap = Net::LDAP.new
    ldap.host = $HOST
    ldap.port = $PORT
    treebase = 'ou=Groups,dc=example,dc=com'
    ldap.bind
    attrs = %w[cn member]
    filter_uid = Net::LDAP::Filter.pres('member')
    ldap.search(base: treebase, attributes: attrs, filter: filter_uid, return_result: true) do |entry|
      entry.attribute_names.each { |_n| entry[_n] }
    end
  end
end
