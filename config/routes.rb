# frozen_string_literal: true

Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post 'auth_user', to: 'ldap_auth#check_ldap_user'
  post 'get_user', to: 'ldap_auth#get_ldap_user'
  post 'get_role', to: 'ldap_auth#user_role'
end
