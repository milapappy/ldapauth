class CreateLdapUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :ldap_users do |t|
      t.string :uid
      t.date :logged_in_time
      t.string :mail
      t.string :role
      t.timestamps
    end
  end
end
